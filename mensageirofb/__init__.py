"""Mensageria do Facebook

Facilita o uso e o teste do serviço de envio do Facebook Messenger e
das configurações do serviço.
"""
from .config import Config
from .messenger import Messenger, TextMessage, AttachmentMessage, \
TextQuickReply, LocationQuickReply
from .storage import Storage, Message
from .user import User
