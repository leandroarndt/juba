"""Erros de requisição ao Facebook"""
import json

class FBError(Exception):
    """Erro de requisição ao Facebook"""
    default = 'Erro ao realizar requisição ao Facebook:\n{data}'

    def __init__(self, message=None, **kwargs):
        if message is None:
            message = self.default
        if 'data' in kwargs:
            kwargs['data'] = json.dumps(kwargs['data'], indent=4)
        self.message = message.format(**kwargs)

    def __str__(self):
        return self.message

class FBUserError(FBError):
    """Erro de solicitação de informações de perfil do usuário"""
    default = 'Erro ao solicitar perfil do usuário {psid} ao Facebook:\n{data}'

class FBContentExceededError(FBError):
    """Erro ao criar mensagem com conteúdo excessivamente grande

    Limites:
    640 caracteres de texto"""
    default = 'Mensagem muito grande: {campo} > {tamanho}'
