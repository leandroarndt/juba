"""Mensageria do Facebook"""
import json
import logging
import requests
from .exceptions import FBError, FBContentExceededError

LOGGER = logging.getLogger(__name__)

class Message(object):
    """Mensagem do Facebook Messenger

    Deve ser iniciada com o `message`, podendo ser acrescentado o
    PSID do usuário, seu número de telefone, o `Messenger`
    e as quick_replies depois.

    Por favor, veja as subclasses para utilização mais prática."""
    def __init__(self,
                 message,
                 psid=None,
                 phone_number=None,
                 first_name=None,
                 last_name=None,
                 sender=None,
                 quick_replies=None,
                 **kwargs):
        """Início de mensagem genérica"""
        self.message, self.sender, self.quick_replies, self.extra = \
        message, sender, quick_replies, kwargs
        if psid or phone_number:
            self.set_recipient(psid, phone_number, first_name, last_name)

    def set_recipient(self, psid=None, phone_number=None, first_name=None, last_name=None):
        if psid:
            self.recipient = {'id': psid}
        elif phone_number:
            self.recipient = {'phone_number': phone_number}
        if first_name:
            self.recipient.update({'first_name': first_name})
        if last_name:
            self.recipient.update({'last_name': last_name})

    def send(self, sender=None):
        """Envia a mensagem

        Se sender não for definido, utiliza o especificado anteriormente"""
        data = {'message': self.message}
        data['message'].update(self.extra)
        if self.quick_replies:
            data['message']['quick_replies'] = [reply.get() for reply in self.quick_replies]
        if not sender:
            return self.sender.send(self.recipient, **data)
        return sender.send(self.recipient, self.message)

class QuickReply(object):
    """Opção de resposta rápida"""
    def __init__(self, content_type='text', title=None, payload=None, image_url=None):
        """Inicia a resposta rápida. Recomenda-se o uso de TextQuickReply e
        LocationQuickReply.

        `title` e `payload` só devem ser definidos se `content_type` for igual a `text`"""
        if content_type != 'text' and (title is not None or payload is not None):
            raise ValueError('title e payload só podem ser definidos para'
                             'resposta do tipo "text"')
        self.content_type, self.title, self.payload, self.image_url = \
        content_type, title, payload, image_url

    def get(self):
        """Retorna a resposta rápida com as restrições apropriadas"""
        data = {
            'content_type': self.content_type,
        }
        if self.image_url:
            data.update({'image_url': self.image_url})
        if self.content_type == 'text':
            data.update({
                'title': self.title,
                'payload': self.payload,
            })
        else:
            data.update({'content_type': 'location'})
        return data

class TextQuickReply(QuickReply):
    """Resposta rápida de texto"""
    def __init__(self, title, payload, image_url=None):
        super(TextQuickReply, self).__init__(
            content_type='text',
            title=title,
            payload=payload,
            image_url=image_url)

class LocationQuickReply(QuickReply):
    """Respsota rápida de localização"""
    def __init__(self, image_url=None):
        super(LocationQuickReply, self).__init__(
            content_type='location',
            image_url=image_url)

class TextMessage(Message):
    """Mensagem de texto simples"""
    def __init__(self, text, *args, **kwargs):
        """Prepara uma mensagem de texto"""
        if len(text) > 640:
            raise FBContentExceededError(campo='text', tamanho='640 caracteres')
        super(TextMessage, self).__init__(message={'text': text}, *args, **kwargs)

class AttachmentMessage(Message):
    """Mensagem com anexo"""
    TYPES = (
        'image',
        'audio',
        'video',
        'file',
        'template',
    )

    #TODO: enviar mensagem estruturada
    def __init__(self,
                 atype,
                 url=None,
                 attachment_id=None,
                 reusable=False,
                 *args, **kwargs):
        """Mensagem com anexo

        `url` ou `attachment_id` precisam ser definidos."""
        if atype not in self.TYPES:
            raise ValueError('{mtype} não é um tipo de anexo conhecido'.format(atype=atype))
        if url is None and attachment_id is None:
            raise ValueError('url e attachment_id não podem ser nulos simultaneamente')
        if url is not None and attachment_id is not None:
            raise ValueError('url e attachment_id não devem ser definidos simultanemanete')
        payload = {'is_reusable': reusable}
        if url:
            payload.update({'url': url})
            self.url = url
            self.attachment_id = None
        if attachment_id:
            payload.update({'attachment_id': attachment_id})
            self.url = None
            self.attachment_id = attachment_id
        self.atype = atype
        super(AttachmentMessage, self).__init__(
            message={'attachment': {
                'type': atype,
                'payload': payload,
            }}, *args, **kwargs)

class Messenger(object):
    """Intermediário do Facebook Messenger"""
    def __init__(self, token, v='2.6', test=False):
        """
        v = versão da API do Facebook Messenger
        token = token de página do Facebook Messenger
        test = utilização para testes (não comunicar com o Facebook)
        """
        self.uri = 'https://graph.facebook.com/v{v}/me/messages?access_token={token}'.format(
            v=v, token=token)
        self.test = test

    def send(self, recipient, **kwargs):
        """Envia mensagem para o Facebook"""
        if 'metadata' in kwargs and len(kwargs['metadata']) > 1000:
            raise FBContentExceededError(campo='metadata', tamanho='1000')
        kwargs.update({'recipient': recipient})
        if not self.test:
            result = json.loads(requests.post(
                self.uri,
                data=json.dumps(kwargs),
                headers={'content-type': 'application/json'}).text)
            if 'error' in result:
                raise FBError(data=result)
            return result
        print('{method} payload:\n{dump}'.format(
            method=self.send.__qualname__,
            dump=json.dumps(kwargs, indent=4)))
        return {'result': 'success'}
