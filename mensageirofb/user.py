"""Informações de usuário de uma página do Facebook"""
import json
import requests
from .exceptions import FBUserError

class User(object):
    """Informações de usuário de uma página do Facebook"""
    
    def __init__(self, psid, token, v='2.6', test=False):
        """
        psid = ID de usuário em uma página do Facebook
        v = versão da API do Facebook Messenger
        token = token de página do Facebook Messenger
        test = utilização para testes (não comunicar com o Facebook)

        *ATENÇÃO:* o _psid_ de usuário é único para cada página, não
        guardando correspondência com o _psid_ do usuário fora desse
        contexto.
        """
        self.psid = psid
        self.uri = 'https://graph.facebook.com/v{v}/{psid}\
?fields=first_name,last_name,profile_pic,locale,timezone,gender\
&access_token={token}'.format(
                v=v, psid=psid, token=token)
        if not test:
            self._perfil = json.loads(requests.get(self.uri).text)
            if 'error' in self._perfil:
                raise FBUserError(psid=psid, data=self._perfil)
        else:
            self._perfil = { # Exemplo da documentação
                "first_name": "Peter",
                "last_name": "Chang",
                "profile_pic": "http://example.com/favicon.png",
                "locale": "en_US",
                "timezone": -7,
                "gender": "male"
            }
    
    def __getattr__(self, name):
        try:
            return self._perfil[name]
        except KeyError:
            raise AttributeError('"{name}" não encontrado no perfil do usuário {psid}'\
                .format(name=name, psid=self.psid))
