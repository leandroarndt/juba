"""Auxilia a tratar as informações recebidas pelo webhook do Faceboko Messanger"""
import json
import logging

LOGGER = logging.getLogger(__name__)

class Storage(object):
    """Trata as mensagens recebidas pelo webhook do Facebook Messenger

    As mensagens são armazenadas na lista `messages`"""
    def __init__(self):
        """Inicializa o objeto"""
        self.messages = []

    def process(self, messages):
        """Analiza as mensagens recebidas e as armazena na lista

        `messages` pode ser string ou dicionário"""
        if isinstance(messages, str):
            messages = json.dumps(messages)
        for entry in messages['entry']:
            for message in entry['messaging']:
                self.messages.append(Message(message))

class Message(object):
    """Mensagem recebida pelo webhook do Facebook Messenger"""
    QUICKREPLY = 'quick_reply'
    POSTBACK = 'postback'
    ATTACHMENTS = 'attachments'
    TEXT = 'text'
    def __init__(self, message):
        """Inicializa a mensagem

        `message` deve ser um dicionário ou uma string."""
        if isinstance(message, str):
            self.data = json.loads(message)
            LOGGER.debug(message)
        else:
            self.data = message
            LOGGER.debug(json.dumps(message))
        self.timestamp = self.data['timestamp']
        self.page_id = self.data['recipient']['id']
        self.sender = self.data['sender']['id']
        self.attachments = []
        self.payload = None

        # Tipo de mensagem
        if self.POSTBACK in self.data:
            self.mtype = self.POSTBACK
            if 'get_started' in self.data[self.POSTBACK]:
                self.payload = self.data[self.POSTBACK]['get_started']['payload']
            else:
                self.payload = self.data[self.POSTBACK]['payload']
            self.message = None
            self.mid = None # self.data[self.POSTBACK]['mid']
            self.seq = None # self.data[self.POSTBACK]['seq']
            if 'title' in self.data[self.POSTBACK]:
                self.text = self.data[self.POSTBACK]['title']
            else:
                self.text = None
        elif 'message' in self.data and self.QUICKREPLY in self.data['message']:
            self.mtype = self.QUICKREPLY
            self.message = self.data['message']
            self.payload = self.message[self.QUICKREPLY]['payload']
            self.mid = self.message['mid']
            self.seq = self.message['seq']
            self.text = self.message['text']
        elif 'message' in self.data and self.ATTACHMENTS in self.data['message']:
            self.mtype = self.ATTACHMENTS
            self.message = self.data['message']
            self.mid = self.message['mid']
            self.attachments = [
                Attachment(att)
                for att
                in self.data['message'][self.ATTACHMENTS]]
        else:
            self.mtype = self.TEXT
            self.message = self.data['message']
            self.payload = None
            self.mid = self.message['mid']
            self.seq = self.message['seq']
            self.text = self.message['text']
        if self.mtype == self.QUICKREPLY and 'coordinates' in self.payload:
            self.coordinates = self.payload['coordinates']
        else:
            self.coordinates = None

class Attachment(object):
    AUDIO = 'audio'
    FALLBACK = 'fallback'
    FILE = 'file'
    IMAGE = 'image'
    LOCATION = 'location'
    VIDEO = 'video'

    def __init__(self, att):
        self.atype = att['type']
        self.latitude = None
        self.longitude = None
        self.title = None
        self.url = None
        if self.atype in (self.AUDIO, self.FILE, self.IMAGE, self.VIDEO):
            self.url = att['payload']['url']
        elif self.atype == self.FALLBACK:
            self.url = att['url']
            self.title = att['title']
        elif self.atype == self.LOCATION:
            self.latitude = att['payload']['coordinates']['lat']
            self.longitude = att['payload']['coordinates']['long']
        else:
            raise NotImplementedError('Attachment type "{mtype}" not implemented'\
                .format(mtype=self.atype))
