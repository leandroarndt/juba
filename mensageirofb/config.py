"""Configuração do Facebook Messenger"""
import json
import requests

class Config(object):
    """Configuração do Facebook Messenger

    Métodos:
    - set: define as configurações conforme o argumento (dicionário)
    - get: retorna as configurações como dicionário
    - erase: apaga as configurações
    """

    def __init__(self, token, v='2.6', test=False):
        """
        v = versão da API do Facebook Messenger
        token = token de página do Facebook Messenger
        test = utilização para testes (não comunicar com o Facebook)
        """
        self.uri = 'https://graph.facebook.com/v{v}/me/messenger_profile?access_token={token}'.format(
            v=v, token=token)
        self.test = test

    def get(self, *args):
        """Retorna a configuração do robô do Facebook Messenger

        Retorna as configurações em formato de dicionário."""
        if not self.test:
            return json.loads(requests.get(self.uri+'&fields='+','.join(args)).text)['data']
        return {'data': []}

    def set(self, **kwargs):
        """Define os valores das configurações

        Retorna a resposta do Facebook como dicionário."""
        if not self.test:
            return json.loads(requests.post(
                self.uri,
                data=json.dumps(kwargs),
                headers={'content-type': 'application/json'}).text)
        return {'result': 'success'}

    def erase(self, *args):
        """Apaga as configurações do Facebook Messenger"""
        if not args:
            data = {
                "fields": [
                    "account_linking_url",
                    "persistent_menu",
                    "get_started",
                    "greeting",
                    "whitelisted_domains",
                    "payment_settings",
                    "target_audience",
                    "home_url"
                ]
            }
        else:
            data = {'fields': []}
            for campo in args:
                data['fields'].append(campo)
        if not self.test:
            return json.loads(requests.delete(
                self.uri,
                data=json.dumps(data),
                headers={'content-type': 'application/json'}).text)
        return {'result': 'success'}
