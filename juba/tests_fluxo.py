"""Simula uma conversa com o robô de ouvidoria."""
import json
import traceback
import functools
from copy import deepcopy
from datetime import datetime
from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.core.management import call_command
from django.conf import settings
from mensageirofb import Message, Messenger, User
from .eouv import nome_do_órgão
from .models import Conversa, Usuário, Fala, Apelido
from . import GET_STARTED, ENCERRAR, DENÚNCIA, ELOGIO, SOLICITAÇÃO, SUGESTÃO, RECLAMAÇÃO
from . import fluxo

def enviar(cls, **kwargs):
    """Envia e salva mensagens

    *cls:* classe da mensagem
    *kwargs:* argumentos para criação da mensagem"""
    mensagem = cls(**kwargs)
    fala = Fala.obter(mensagem)

fluxo.enviar = enviar
mess = Messenger.__init__
Messenger.__init__ = functools.partialmethod(mess, test=True)
use = User.__init__
User.__init__ = functools.partialmethod(use, test=True)

class Conversas(TestCase):
    """Testa o passo-a-passo de uma conversa no Facebook Messenger de acordo
    com o fluxo desenhado.

    Sobre a forma de execução, ver o [stackoverflow](https://stackoverflow.com/questions/5387299/python-unittest-testcase-execution-order)"""
    conversa = None
    seq = 0

    def setUp(self):
        self.comum = {
            'object': 'page',
            'entry': [{
                'id': 4321,
                'time': 1458692752478,
                'messaging': [{
                    'sender': {
                        'id': 1234
                    },
                    'recipient': {
                        'id': 4321
                    },
                }]
            }],
        }
        self.url = reverse(':'.join((__package__, 'webhook')))
        self.client = Client()
        self.seq = 0

    def get_json(self, mensagem):
        retorno = deepcopy(self.comum)
        retorno['entry'][0]['messaging'][0].update(mensagem)
        retorno['entry'][0]['messaging'][0]['timestamp'] = str(datetime.now())
        return retorno

    def get_message_json(self, mensagem):
        self.seq += 1
        message = {
            'message': {
                'mid': 'mid{}'.format(self.seq),
                'seq': self.seq
            }
        }
        message['message'].update(mensagem)
        return self.get_json(message)

    def passo_1_get_started(self):
        """Passo 1.1: botão "começar"

        Tipo de fala: `postback`

        Deve fechar conversa anterior e iniciar outra."""
        print('{0}: get_started'.format(self.__class__.__name__), end=' → ')
        self.client.post(
            self.url,
            data=json.dumps(self.get_json({
                Message.POSTBACK: {
                    'get_started': {
                        'payload': GET_STARTED
                    }
                }
            })),
            content_type='application/json'
        )
        self.assertEqual(len(Conversa.objects.filter(usuário__psid=1234, ativa=True)), 1)
        conversa = Conversa.objects.get(usuário__psid=1234, ativa=True)
        self.assertEqual(conversa.etapa, Conversa.INÍCIO)
        self.assertEqual(
            conversa.fala_set.order_by('-momento').all()[0].etapa,
            Fala.OUTRA
        )
        print('Ok', flush=True)

    def passo_2_árvore(self):
        """Passo 1.2: seleção do tipo de interação — manifestação ou FAQ

        Tipo de fala: `postback`"""
        pass

    def passo_3_tipo(self):
        """Passo 2: tipos de manifestação"""
        print('{0}: tipo de manifestação'.format(self.__class__.__name__), end=' → ')
        self.client.post(
            self.url,
            data=json.dumps(self.get_message_json({
                'text':'Denunciar',
                Message.QUICKREPLY: {
                    'payload': DENÚNCIA
                }
            })),
            content_type='application/json'
        )
        conversa = Conversa.objects.get(usuário__psid=1234, ativa=True)
        self.assertEqual(conversa.tipo, Conversa.DENÚNCIA)
        self.assertEqual(
            conversa.fala_set.order_by('-momento').filter(enviada=False)[0].etapa,
            Fala.TIPO
        )
        self.client.post(
            self.url,
            data=json.dumps(self.get_message_json({
                'text':'Elogiar',
                Message.QUICKREPLY: {
                    'payload': ELOGIO
                }
            })),
            content_type='application/json'
        )
        conversa = Conversa.objects.get(usuário__psid=1234, ativa=True)
        self.assertEqual(conversa.tipo, Conversa.ELOGIO)
        self.assertEqual(
            conversa.fala_set.order_by('-momento').filter(enviada=False)[0].etapa,
            Fala.TIPO
        )
        self.client.post(
            self.url,
            data=json.dumps(self.get_message_json({
                'text':'Solicitar',
                Message.QUICKREPLY: {
                    'payload': SOLICITAÇÃO
                }
            })),
            content_type='application/json'
        )
        conversa = Conversa.objects.get(usuário__psid=1234, ativa=True)
        self.assertEqual(conversa.tipo, Conversa.SOLICITAÇÃO)
        self.assertEqual(
            conversa.fala_set.order_by('-momento').filter(enviada=False)[0].etapa,
            Fala.TIPO
        )
        self.client.post(
            self.url,
            data=json.dumps(self.get_message_json({
                'text':'Sugerir',
                Message.QUICKREPLY: {
                    'payload': SUGESTÃO
                }
            })),
            content_type='application/json'
        )
        conversa = Conversa.objects.get(usuário__psid=1234, ativa=True)
        self.assertEqual(conversa.tipo, Conversa.SUGESTÃO)
        self.assertEqual(
            conversa.fala_set.order_by('-momento').filter(enviada=False)[0].etapa,
            Fala.TIPO
        )
        self.client.post(
            self.url,
            data=json.dumps(self.get_message_json({
                'text':'Reclamar',
                Message.QUICKREPLY: {
                    'payload': RECLAMAÇÃO
                }
            })),
            content_type='application/json'
        )
        conversa = Conversa.objects.get(usuário__psid=1234, ativa=True)
        self.assertEqual(conversa.tipo, Conversa.RECLAMAÇÃO)
        self.assertEqual(
            conversa.fala_set.order_by('-momento').filter(enviada=False)[0].etapa,
            Fala.TIPO
        )
        self.assertEqual(len(Conversa.objects.filter(usuário__psid=1234, ativa=True)), 1)
        print('Ok', flush=True)

    def passo_40_órgão(self):
        """Seleciona o órgão da manifestação"""
        print('{0}: órgão'.format(self.__class__.__name__), end=' → ')
        self.client.post(
            self.url,
            data=json.dumps(self.get_message_json({
                'text':'Controladoria-Geral da União'
            })),
            content_type='application/json'
        )
        conversa = Conversa.objects.get(usuário__psid=1234, ativa=True)
        self.assertTrue(nome_do_órgão(conversa.órgão).startswith('CGU'))
        print('Ok')

    def passo_41_órgão_apelido(self):
        """Seleciona o órgão por apelido"""
        print('{0}: órgão por apelido'.format(self.__class__.__name__), end=' → ')
        Apelido(apelido='DHV', oficial='CGU').save()
        conversa = Conversa.objects.get(usuário__psid=1234, ativa=True)
        conversa.etapa = Conversa.TIPO
        conversa.órgão = None
        conversa.save()
        self.client.post(
            self.url,
            data=json.dumps(self.get_message_json({
                'text':'DHV'
            })),
            content_type='application/json'
        )
        conversa = Conversa.objects.get(usuário__psid=1234, ativa=True)
        self.assertTrue(nome_do_órgão(conversa.órgão).startswith('CGU'))
        print('Ok')

    def passo_5_texto(self, iteração=1):
        """Lorem ipsum"""
        print('{0}: conteúdo de texto'.format(self.__class__.__name__), end=' → ')
        self.client.post(
            self.url,
            data=json.dumps(self.get_message_json({
                'text':'Linha inserida pelo usuário nº {}.'.format(iteração)
            })),
            content_type='application/json'
        )
        conversa = Conversa.objects.get(usuário__psid=1234, ativa=True)
        fala = conversa.fala_set.order_by('-momento').all()[1]
        self.assertEqual(conversa.etapa, Conversa.CONTEÚDO_INSERIDO)
        self.assertEqual(fala.etapa, Fala.CONTEÚDO)
        self.assertEqual(fala.texto, 'Linha inserida pelo usuário nº {}.'.format(iteração))
        print('Ok')

    def passo_6_anexo(self):
        """More lorem ipsum"""
        print('{0}: conteúdo com anexo'.format(self.__class__.__name__), end=' → ')
        self.client.post(
            self.url,
            data=json.dumps(self.get_message_json({
                'attachments': [{
                    "type": "image",
                    "payload": {
                        "url": "https://cdn.fbsbx.com/v/t59.2708-21/22551162_"
                        "900930300063643_7247140451617079296_n.gif?oh=523fcbe"
                        "e810eb24178a1bdae6fd881ae&oe=5A088558"
                    }
                }]
            })),
            content_type='application/json'
        )
        conversa = Conversa.objects.get(usuário__psid=1234, ativa=True)
        fala = conversa.fala_set.order_by('-momento').all()[1]
        self.assertEqual(conversa.etapa, Conversa.CONTEÚDO_INSERIDO)
        self.assertEqual(fala.etapa, Fala.CONTEÚDO)
        self.assertEqual(fala.anexos.count(), 1)
        self.assertEqual(
            fala.anexos.all()[0].url,
            "https://cdn.fbsbx.com/v/t59.2708-21/22551162_"
            "900930300063643_7247140451617079296_n.gif?oh=523fcbe"
            "e810eb24178a1bdae6fd881ae&oe=5A088558"
        )
        print('Ok')

    def passo_7_encerrar_pelo_usuário(self):
        """Encerra a manifestação"""
        print('{0}: encerramento pelo usuário'.format(self.__class__.__name__), end=' → ')
        conversa = Conversa.objects.get(usuário__psid=1234, ativa=True)
        self.client.post(
            self.url,
            data=json.dumps(self.get_message_json({
                'text':'Encerrar',
                Message.QUICKREPLY: {
                    'payload': ENCERRAR
                }
            })),
            content_type='application/json'
        )
        conversa = Conversa.objects.get(pk=conversa.pk)
        self.assertFalse(conversa.ativa)
        self.assertIsNotNone(conversa.protocolo)
        try:
            conversa = Conversa.objects.get(usuário__psid=1234, ativa=True)
        except ObjectDoesNotExist:
            pass
        else:
            raise Exception('Há uma fala ativa')
        print('Ok')

    def passo_8_encerrar_por_inatividade(self):
        """Encerra a manifestação por inatividade"""
        print('{0}: encerramento por inatividade'.format(self.__class__.__name__), end=' → ')
        usuários = (
            Usuário(psid=1),
            Usuário(psid=2),
            Usuário(psid=3),
            Usuário(psid=4),
            Usuário(psid=5),
        )
        for usuário in usuários:
            usuário.save()
        início = Conversa(
            usuário=usuários[0],
            etapa=Conversa.INÍCIO
        )
        início.save()
        tipo = Conversa(
            usuário=usuários[1],
            etapa=Conversa.TIPO
        )
        tipo.save()
        órgão = Conversa(
            usuário=usuários[2],
            etapa=Conversa.ÓRGÃO
        )
        órgão.save()
        conteúdo = Conversa(
            usuário=usuários[3],
            etapa=Conversa.CONTEÚDO_INSERIDO
        )
        conteúdo.save()
        ativa = Conversa(
            usuário=usuários[4],
            etapa=Conversa.CONTEÚDO_INSERIDO
        )
        ativa.save()
        Fala(
            conversa=início,
            texto='antiga'
        ).save()
        Fala(
            conversa=tipo,
            texto='antiga'
        ).save()
        Fala(
            conversa=órgão,
            texto='antiga'
        ).save()
        Fala(
            conversa=conteúdo,
            texto='antiga'
        ).save()
        Fala(
            conversa=ativa,
            texto='nova'
        ).save()
        Fala.objects.filter(texto='antiga').update(momento=datetime.now()-2*settings.JUBA_TIMEDELTA)
        call_command('desconversar')
        início = Conversa.objects.get(usuário__psid=1)
        self.assertFalse(início.ativa)
        self.assertEqual(início.protocolo, '')
        tipo = Conversa.objects.get(usuário__psid=2)
        self.assertFalse(tipo.ativa)
        self.assertEqual(tipo.protocolo, '')
        órgão = Conversa.objects.get(usuário__psid=3)
        self.assertFalse(órgão.ativa)
        self.assertEqual(órgão.protocolo, '')
        conteúdo = Conversa.objects.get(usuário__psid=4)
        self.assertFalse(conteúdo.ativa)
        self.assertNotEqual(conteúdo.protocolo, '')
        ativa = Conversa.objects.get(usuário__psid=5)
        self.assertTrue(ativa.ativa)
        self.assertEqual(ativa.protocolo, '')
        print('Ok')

    def passos(self):
        for name in sorted(dir(self)):
            if name.startswith("passo_"):
                yield name, getattr(self, name)

    def test_conversa(self):
        """Testa a conversa de maneira organizada"""
        for nome, passo in self.passos():
            try:
                passo()
            except Exception as e:
                # A primeira falha invalida os demais passos
                traceback.print_exc()
                self.fail('{} falhou ({}: {})'.format(nome, type(e), e))
