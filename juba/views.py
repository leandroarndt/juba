"""Visão associadas ao Facebook Messenger"""
import json
import sys
import logging
import traceback
import threading
from django.conf import settings
from django.views.generic import View
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from mensageirofb import AttachmentMessage, TextMessage, Messenger, \
     TextQuickReply, LocationQuickReply, Storage, Message
from .fluxo import processar

LOGGER = logging.getLogger(__name__)

def throw_error(exc_info):
    """Joga o erro no sys.stderr"""
    LOGGER.error('ERRO!')
    LOGGER.error(exc_info()[0])
    LOGGER.error(exc_info()[1])
    LOGGER.error(traceback.format_tb(exc_info()[2]))

class Webhook(View):
    """Hook para o Facebook Messenger

    De acordo com a
    [especificação](https://developers.facebook.com/docs/messenger-platform/webhook#response)
    do FB Messenger, deve sempre retornar com o código HTTP 200."""

    mensageiro = Messenger(token=settings.JUBATOKEN)
    storage = Storage()

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(Webhook, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        """Onde o Facebook verifica o aplicativo"""
        if 'hub.verify_token' in request.GET and \
          request.GET['hub.verify_token'] == settings.FBTOKEN:
            return HttpResponse(request.GET['hub.challenge'])
        return HttpResponse('ERRO')

    def post(self, request):
        """Onde o trabalho verdadeiro do hook acontece

        Documentação do formato da requisição do Facebook:
        https://developers.facebook.com/docs/messenger-platform/webhook#format"""
        body = request.body
        try:
            data = json.loads(body)
        except json.decoder.JSONDecodeError:
            LOGGER.error(request.body, file=sys.stderr, flush=True)
            throw_error(sys.exc_info)
            return HttpResponse('JSON não encontrado')
        except:
            LOGGER.error('ERRO DESCONHECIDO!')
            throw_error(sys.exc_info)
            return HttpResponse('JSON não encontrado')
        try:
            self.storage.process(data)
        except:
            LOGGER.error('ERRO DESCONHECIDO!')
            throw_error(sys.exc_info)
            return HttpResponse('Impossível converter em mensagens')
        try:
            threading.Thread(
                target=processar,
                kwargs={
                    'storage': self.storage,
                    'mensageiro': self.mensageiro,
                },
                daemon=False).run()
        except:
            throw_error(sys.exc_info)
        return HttpResponse('Success')
