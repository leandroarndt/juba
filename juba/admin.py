from django.contrib import admin
from .models import Conversa, Fala, Usuário, Anexo, Apelido

admin.site.register(Conversa)
admin.site.register(Fala)
admin.site.register(Usuário)
admin.site.register(Anexo)
admin.site.register(Apelido)
