"""Modelos de conversas de ouvidoria no Facebook"""
import logging
from typing import List
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from mensageirofb.storage import Message as Recebida
from mensageirofb.messenger import \
    Message as Enviada, \
    TextMessage
from mensageirofb import User

LOGGER = logging.getLogger(__name__)

class Conversa(models.Model):
    """Armazena conversas e seus elementos

    - *usuário:* chave externa para `Usuário`
    - *órgão:* órgão de que trata a conversa (nulo, se não identificado)
    - *ativa:* se a conversa está ativa ou já foi encerrada
    - *sincronizada:* se a conversa já foi sincronizada com o sistema e-OUV
    - *última:* data e hora da última interação na conversa

    Sobre a identificação do órgão, uma boa pedida é a
    [difflib](https://docs.python.org/3/library/difflib.html)"""
    INÍCIO = 0
    TIPO = 1
    ÓRGÃO = 2
    # ESPERA_CONTEÚDO = 3
    CONTEÚDO_INSERIDO = 4

    DENÚNCIA = 1
    ELOGIO = 2
    RECLAMAÇÃO = 3
    SOLICITAÇÃO = 4
    SUGESTÃO = 5

    usuário = models.ForeignKey('Usuário')
    tipo = models.IntegerField(default=0)
    órgão = models.IntegerField(null=True)
    etapa = models.IntegerField(default=INÍCIO)
    ativa = models.BooleanField(default=True)
    protocolo = models.CharField(max_length=24, default='')

    def __str__(self):
        if self.ativa:
            atividade = 'ativa'
        else:
            atividade = 'encerrada'
        if self.protocolo:
            protocolo = self.protocolo
        else:
            protocolo = 'não sincronizada'
        if self.órgão:
            return '{usuário} sobre {órgão}, {atividade}, {protocolo}'\
                .format(
                    usuário=self.usuário,
                    órgão=self.get_órgão_display(),
                    atividade=atividade,
                    protocolo=protocolo
                )
        return '{usuário}, {atividade}, {protocolo}'\
            .format(
                usuário=self.usuário,
                atividade=atividade,
                protocolo=protocolo
            )

    def get_conteúdo(self) -> str:
        """Retorna o texto da manifestação"""
        return '\r\n\r\n'.join([
            fala.texto
            for fala
            in self.fala_set.filter(
                tipo=Recebida.TEXT,
                etapa=Fala.CONTEÚDO
            ).order_by('momento')
        ])

    def get_conversa(self) -> str:
        """Retorna a conversa toda"""
        return '\r\n\r\n'.join([
            fala.get_texto()
            for fala
            in self.fala_set.all().order_by('momento')
        ])

    def get_órgão_display(self):
        """Retorna o nome do órgão por extenso"""
        from .eouv import nome_do_órgão # Importar aqui evita recursão
        return nome_do_órgão(self.órgão)

    def get_anexos(self) -> List:
        """Retorna todos os anexos das falas"""
        anexos = []
        for fala in self.fala_set.all():
            anexos.extend(fala.get_anexos())
        return anexos

    @classmethod
    def obter(cls, psid):
        """Retorna a conversa ativa para o usuário `psid`"""
        return cls.objects.get(usuário__psid=psid, ativa=True)

class Fala(models.Model):
    """Armazena os elementos da conversa

    - *conversa:* chave externa para `Conversa`
    - *texto:* texto da interação
    - *momento:* data e hora da interação
    - *tipo:* tipo de interação
    - *etapa:* a que etapa da conversa a fala corresponde"""
    # pylint: disable=R0902
    OUTRA = 0
    TIPO = 1
    ÓRGÃO = 2
    CONTEÚDO = 3

    conversa = models.ForeignKey('Conversa')
    texto = models.TextField()
    etapa = models.IntegerField(default=OUTRA)
    mid = models.CharField(max_length=128)
    seq = models.CharField(max_length=128)
    payload = models.CharField(max_length=20)
    tipo = models.CharField(max_length=20)
    page_id = models.BigIntegerField(null=True) # Informação só existe nas recebidas
    momento = models.DateTimeField(auto_now_add=True)
    enviada = models.BooleanField(default=False)
    anexos = models.ManyToManyField('Anexo')

    def __str__(self):
        if self.enviada:
            sentido = 'para'
        else:
            sentido = 'de'
        return '{sentido} {usuário} {momento}'.format(
            sentido=sentido,
            usuário=self.conversa.usuário,
            momento=self.momento
        )

    def get_texto(self):
        """Texto inteligível para o usuário do e-OUV

        Difere de Fala.__str__ porque este se destina ao desenvolvedor ou
        usuário do ambiente de administração do Django.
        Fala.get_texto, em vez disso, se destina a criar o
        anexo da conversa no envio ao e-OUV."""
        if self.texto:
            texto = self.texto
        elif self.payload:
            texto = f'[{self.payload}]'
        elif self.anexos.count():
            texto = '\n'.join([
                anexo.get_texto()
                for anexo in self.anexos.all()
            ])
        if self.enviada:
            return f'*JUBA:* {texto}'
        return f'*{self.conversa.usuário.first_name} ' \
        f'{self.conversa.usuário.last_name}:* {texto}'

    def get_anexos(self) -> List:
        return [anexo for anexo in self.anexos.all()]

    @classmethod
    def obter(cls, mensagem, **kwargs):
        """Armazena a mensagem no banco de dados

        Argumento opcional: etapa (etapa da conversa a que se refere
        a fala."""
        # pylint: disable=R0912
        nova = cls(
            texto='',
            mid='',
            seq='',
            payload=''
        )
        if isinstance(mensagem, Recebida):
            # try:
            #     usuário = Usuário.objects.get(psid=mensagem.sender)
            # except ObjectDoesNotExist:
            #     usuário = Usuário.obter(mensagem.sender)
            usuário = Usuário.obter(mensagem.sender)
            nova.conversa, início = Conversa.objects.get_or_create(
                usuário=usuário,
                ativa=True,
                defaults={
                    'usuário':usuário,
                    'ativa':True
                }
            )
            # try:
            #     nova.conversa = Conversa.objects.get(usuário=usuário.psid, ativa=True)
            # except ObjectDoesNotExist:
            #     conversa = Conversa(usuário=usuário.psid)
            #     conversa.save()
            #     nova.conversa = conversa
            if início:
                nova.conversa.save()
            if hasattr(mensagem, 'text') and mensagem.text:
                nova.texto = mensagem.text
            nova.tipo = mensagem.mtype
            nova.page_id = mensagem.page_id
            if hasattr(mensagem, 'mid') and mensagem.mid:
                nova.mid = mensagem.mid
            if hasattr(mensagem, 'seq') and mensagem.seq:
                nova.seq = mensagem.seq
            if hasattr(mensagem, 'payload') and mensagem.payload:
                nova.payload = mensagem.payload
        elif isinstance(mensagem, Enviada):
            usuário = Usuário.obter(mensagem.recipient['id'])
            nova.enviada = True
            nova.conversa, início = Conversa.objects.get_or_create(
                usuário=usuário,
                ativa=True,
                defaults={
                    'usuário':usuário,
                    'ativa':True
                }
            )
            if início:
                nova.conversa.save()
            if isinstance(mensagem, TextMessage):
                nova.tipo = Recebida.TEXT
                nova.texto = mensagem.message['text']
            else:
                nova.tipo = Recebida.ATTACHMENTS
        if 'etapa' in kwargs:
            nova.etapa = kwargs['etapa']
        nova.save()

        # Processa os anexos
        if nova.enviada and hasattr(mensagem, 'url'):
            nova.anexos.add(Anexo.armazenar(mensagem))
            nova.save()
        elif hasattr(mensagem, 'attachments'):
            for anexo in mensagem.attachments:
                nova.anexos.add(Anexo.armazenar(anexo))
            nova.save()

        return nova

class Usuário(models.Model):
    """Armazena informações do usuário

    Usa-se TextField para evitar problemas no dimensionamento de um campo de caracteres."""
    psid = models.BigIntegerField(primary_key=True)
    first_name = models.TextField()
    last_name = models.TextField()
    profile_pic = models.TextField()
    locale = models.CharField(max_length=5)
    timezone = models.IntegerField(null=True)
    gender = models.CharField(max_length=10)

    def __str__(self):
        return '{nome} {sobrenome}'.format(nome=self.first_name, sobrenome=self.last_name)

    @classmethod
    def obter(cls, usuário):
        """Armazena as informações do usuário da página do Facebook"""
        objeto = None
        if isinstance(usuário, int):
            try:
                objeto = cls.objects.get(psid=usuário)
            except ObjectDoesNotExist:
                pass
        if not isinstance(usuário, User):
            usuário = User(usuário, settings.JUBATOKEN)
        if objeto is None:
            objeto = Usuário(
                psid=usuário.psid,
                first_name=usuário.first_name,
                last_name=usuário.last_name,
                profile_pic=usuário.profile_pic,
                locale=usuário.locale,
                timezone=usuário.timezone,
                gender=usuário.gender
            )
        else:
            objeto.first_name, objeto.last_name, objeto.profile_pic, objeto.locale, \
            objeto.timezone, objeto.gender = \
            usuário.first_name, usuário.last_name, usuário.profile_pic, usuário.locale, \
            usuário.timezone, usuário.gender
        objeto.save()
        return objeto

class Anexo(models.Model):
    """Armazena anexos do Facebook Messenger"""
    tipo = models.CharField(max_length=20)
    url = models.URLField()
    texto = models.TextField()
    latitude = models.DecimalField(max_digits=10, decimal_places=7, null=True)
    longitude = models.DecimalField(max_digits=10, decimal_places=7, null=True)

    @classmethod
    def armazenar(cls, anexo):
        """Armazena anexo de mensagem"""
        novo = Anexo(
            url='',
            texto=''
        )
        if isinstance(anexo, Enviada):
            novo.tipo = anexo.atype
            if anexo.url:
                novo.url = anexo.url
            else:
                LOGGER.error('attachment_id em mensagens enviadas '
                             'não implementado')
                novo.texto = 'Anexo previamente enviado. '
                'attachment_id: {attachment_id}'\
                    .format(attachment_id=anexo.attachment_id)
        else:
            if anexo.url:
                novo.url = anexo.url
            if anexo.title:
                novo.texto = anexo.title
            if anexo.latitude:
                novo.latitude = anexo.latitude
            if anexo.longitude:
                novo.longitude = anexo.longitude
            novo.tipo = anexo.atype
        novo.save()
        return novo

    def get_texto(self):
        """Retorna texto útil para o histórico da conversa no e-OUV"""
        if self.texto:
            return f'{self.texto} {self.url}'
        if self.url:
            return self.url
        return f'Latitude: {self.latitude}, longitude: {self.longitude}.'

class Apelido(models.Model):
    """Permite interpretar formas comuns de se referir a certos órgãos públicos.abs

    Exemplos: Caixa -> CEF - Caixa Econômica Federal,
    INPS -> INSS - Instituto Nacional do Seguro Social"""
    apelido = models.CharField(max_length=200)
    oficial = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.apelido} ({self.oficial})'