from django.apps import AppConfig


class FbmessengerConfig(AppConfig):
    name = 'fbmessenger'
