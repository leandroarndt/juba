# pylint: disable=C0111
import traceback
import functools
from datetime import datetime
from django.test import TestCase
from mensageirofb import TextMessage, AttachmentMessage, User, Messenger
from mensageirofb.storage import Message
from .models import Conversa, Fala, Usuário

mess = Messenger.__init__
Messenger.__init__ = functools.partialmethod(mess, test=True)
use = User.__init__
User.__init__ = functools.partialmethod(use, test=True)

class Models(TestCase):
    def passo_1_usuário(self):
        print('{0}: usuário'.format(self.__class__.__name__), end=' → ')
        usuário_fb = User(1234, '4321', test=True)
        usuário_models = Usuário.obter(usuário_fb)
        self.assertEqual(usuário_models.psid, usuário_fb.psid)
        self.assertEqual(usuário_models.first_name, usuário_fb.first_name)
        self.assertEqual(usuário_models.last_name, usuário_fb.last_name)
        self.assertEqual(usuário_models.profile_pic, usuário_fb.profile_pic)
        self.assertEqual(usuário_models.locale, usuário_fb.locale)
        self.assertEqual(usuário_models.timezone, usuário_fb.timezone)
        self.assertEqual(usuário_models.gender, usuário_fb.gender)
        print('Ok', flush=True)

    def passo_2_text_message(self):
        # pylint: disable=E1101
        print('{0}: mensagem de texto recebida'.format(self.__class__.__name__), end=' → ')
        mensagem = Message({
            'sender': {'id': 1234},
            'recipient': {'id': 4321},
            'timestamp': str(datetime.now()),
            'message': {
                'mid': 'mid1',
                'seq': '1',
                'text': 'Mensagem de texto',
            }
        })
        Fala.obter(mensagem)
        fala = Fala.objects.get(mid='mid1')
        self.assertEqual(fala.conversa.usuário.pk, 1234)
        self.assertEqual(fala.texto, 'Mensagem de texto')
        self.assertEqual(fala.mid, 'mid1')
        self.assertEqual(fala.seq, '1')
        self.assertFalse(fala.payload)
        self.assertEqual(fala.tipo, Message.TEXT)
        self.assertEqual(fala.page_id, 4321)
        self.assertFalse(fala.enviada)
        print('Ok', flush=True)

    def passo_3_anexo(self):
        # pylint: disable=E1101
        print('{0}: mensagem com anexo recebida'.format(self.__class__.__name__), end=' → ')
        mensagem = Message({
            'sender': {'id': 1234},
            'recipient': {'id': 4321},
            'timestamp': str(datetime.now()),
            'message': {
                'mid': 'mid2',
                'seq': '2',
                'attachments': [
                    {
                        'type': 'image',
                        'payload': {'url': 'https://farm5.staticflickr.com/'
                                           '4464/37101531514_55c90cc6d9_m_d.jpg'}
                    },
                    {
                        'type': 'fallback',
                        'url': 'http://www.ouvidorias.gov.br/',
                        'title': 'Portal Ouvidorias.gov',
                    },
                    {
                        'type': 'location',
                        'payload': {
                            'coordinates': {
                                'lat': -23,
                                'long': 40
                            }
                        }
                    }
                ]
            }
        })
        Fala.obter(mensagem)
        fala = Fala.objects.get(mid='mid2')
        self.assertEqual(fala.conversa.usuário.pk, 1234)
        self.assertEqual(fala.tipo, Message.ATTACHMENTS)
        self.assertEqual(fala.page_id, 4321)
        self.assertFalse(fala.enviada)
        imagem = fala.anexos.get(tipo='image')
        self.assertEqual(imagem.url, 'https://farm5.staticflickr.com/'
                         '4464/37101531514_55c90cc6d9_m_d.jpg')
        print('Ok', flush=True)
        print('Ok', flush=True)

    def passo_4_text_message(self):
        pass

    def passo_5_attachment_message(self):
        pass

    def passo_6_conversa(self):
        # pylint: disable=E1101
        print('{0}: conversa'.format(self.__class__.__name__), end=' → ')
        conversa = Conversa.objects.get()
        self.assertEqual(conversa.usuário.pk, 1234)
        self.assertFalse(conversa.órgão)
        self.assertTrue(conversa.ativa)
        self.assertEqual(conversa.protocolo, '')
        print('Ok', flush=True)

    def passos(self):
        for name in sorted(dir(self)):
            if name.startswith("passo_"):
                yield name, getattr(self, name)

    def test_models(self):
        """Testa a conversa de maneira organizada"""
        for nome, passo in self.passos():
            try:
                passo()
            except Exception as e:
                # A primeira falha invalida os demais passos
                traceback.print_exc()
                self.fail('{} falhou ({}: {})'.format(nome, type(e), e))