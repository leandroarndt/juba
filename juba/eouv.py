"""Integração com o e-OUV

Aqui você encontra:

- Lista de órgãos
- Envio de manifestações e recebimento de protocolo

Lista de stopwords obtida de
https://anoncvs.postgresql.org/cvsweb.cgi/pgsql/src/backend/snowball/stopwords/portuguese.stop?rev=1.1;content-type=text%2Fplain"""
import logging
import re
import os
import sys
import gzip
import codecs
from django.utils.timezone import datetime
from django.core.exceptions import ObjectDoesNotExist
from difflib import get_close_matches
from typing import Dict, Tuple
from requests import get
from zeep import Client
from django.conf import settings
from .models import Conversa, Apelido

LOGGER = logging.getLogger(__name__)

class EOUVError(Exception):
    """Qualquer erro na comunicação com o e-OUV"""
    pass

class _lista_de_órgãos(object):
    """Retorna o dicionário de órgãos do SIORG"""
    _atualização = datetime(2000,1,1)
    _dicionário = {}

    def __init__(self):
        self._atualiza()

    @classmethod
    def __call__(cls) -> Dict[int,str]:
        """Retorna o dicionário de órgãos registrados no e-OUV."""
        if datetime.now()-cls._atualização>settings.SIORG_TIMEDELTA:
            cls._atualiza()
        return cls._dicionário

    @classmethod
    def _atualiza(cls) -> Dict[int,str]:
        """Atualiza o dicionário de órgãos"""
        lista = EOUV['consulta dados'].GetListaOrgaosSiorg(settings.EOUV_USUARIO, settings.EOUV_SENHA)
        if lista['CodigoErro'] != 0:
            raise EOUVError(f'{lista["CodigoErro"]}: {lista["DescricaoErro"]}')
        lista = lista['OrgaosSiorgOuvidoria']['OrgaoSiorgOuvidoria']
        cls._atualização = datetime.now()
        cls._dicionário = {órgão['CodOrgao']: órgão['NomOrgao'] for órgão in lista}

# Evita requisições desnecessárias ao e-OUV
if {
        # Comandos do Django
        'check',
        'compilemessages',
        'diffsettings',
        'makemessages',
        'makemigrations',
        'migrate',
        'sendtestemail',
        'showmigrations',
        'sqlmigrate',
        'squashmigrations',
        'startapp',
        'changepassword',
        'createsuperuser',
        'remove_stale_contenttypes',
        'collectstatic',
        'findstatic',
        # Comandos de django-extensions
        'admin_generator',
        'clean_pyc',
        'create_app',
        'create_command',
        'create_template_tags',
        'create_jobs',
        'clear_cache',
        'compile_pyc',
        'describe_form',
        'delete_squashed_migrations',
        'dumpscript',
        'dumpscript',
        'find_template',
        'generate_secret_key',
        'graph_models',
        'mail_debug',
        'notes',
        'passwd',
        'pipchecker',
        'print_settings',
        'print_user_for_session',
        'drop_test_database',
        'reset_db',
        'show_template_tags',
        'show_urls',
        'sqldiff',
        'sqlcreate',
        'sqldsn',
        'sync_s3',
        'unreferenced_files',
        'validate_templates'
}.intersection(set(sys.argv)):
    print('e-OUV não será consultado durante o comando.', flush=True)
else:
    try:
        EOUV
    except NameError:
        LOGGER.debug('Conhecendo os serviços do e-OUV…')
        EOUV = {
            'consulta dados': Client(
                'http://treinamentoouvidorias.cgu.gov.br/servicos/'
                'ServicoConsultaDadosCodigos.svc?wsdl'
            ).service,
            'mantém manifestação': Client(
                'http://treinamentoouvidorias.cgu.gov.br/servicos/'
                'ServicoManterManifestacao.svc?wsdl'
            ).service,
            # 'consulta manifestação': Client(
            #     'http://treinamentoouvidorias.cgu.gov.br/servicos/'
            #     'ServiceConsultaManifestacao.svc?wsdl'
            # ).service
        }
        LOGGER.debug('Serviços do e-OUV conhecidos.')
        # Correspondência de tipos de manifestação entre Conversa e EOUV
        _tipos = {
            tipo['DescTipoManifestacao']: tipo['IdTipoManifestacao']
            for tipo
            in EOUV['consulta dados'].GetListaTiposManifestacao(
                settings.EOUV_USUARIO, settings.EOUV_SENHA
            )['TiposManifestacao']['TipoManifestacao']
        }
        EOUV['tipos'] = {
            Conversa.DENÚNCIA: _tipos['Denúncia'],
            Conversa.ELOGIO: _tipos['Elogio'],
            Conversa.SOLICITAÇÃO: _tipos['Solicitação'],
            Conversa.SUGESTÃO: _tipos['Sugestão'],
            Conversa.RECLAMAÇÃO: _tipos['Reclamação'],
            0: _tipos['Não Classificada']
        }
        lista_de_órgãos = _lista_de_órgãos()

with open(os.path.join(os.path.dirname(__file__), 'stopwords.txt'), 'r', encoding='utf-8') as f:
    _stopwords = f.read().upper().splitlines()

def obter_órgão(nome: str):
    """Retorna o órgão a partir do nome aproximado"""
    órgãos = lista_de_órgãos()
    órgãos_maíusculas = {nome.upper(): cod for cod, nome in órgãos.items()}
    try:
        nome = Apelido.objects.get(apelido=nome).oficial
    except ObjectDoesNotExist:
        pass
    # Primeiro testa as siglas:
    siglas = {nome.split(' ')[0]: cod for cod, nome in órgãos.items()}
    melhor = get_close_matches(nome.upper(), siglas.keys(), n=1)
    if melhor:
        return siglas[melhor[0]], órgãos[siglas[melhor[0]]]
    # Se não for confiável (>0.6), tenta pelas partes do nome
    partes = re.compile(r'(\w+)')
    partes_nome = [
        parte.upper() for parte
        in partes.findall(nome)
        if parte.upper() not in _stopwords
    ]
    partes_órgãos = {}
    for código, nome in órgãos.items():
        partes_órgão = [
            parte.upper() for parte
            in partes.findall(nome)
            if parte != '' and parte.upper() not in _stopwords
        ]
        if len(partes_órgão) >= len(partes_nome):
            for início in range(len(partes_órgão)-len(partes_nome)+1):
                partes_órgãos[' '.join(partes_órgão[início:início+len(partes_nome)]).upper()]=código
    melhor = get_close_matches(' '.join(partes_nome).upper(), partes_órgãos.keys(), n=1)
    if melhor:
        return partes_órgãos[melhor[0]], órgãos[partes_órgãos[melhor[0]]]
    # se ainda não for confiável o suficiente (0.6), tenta pelo nome:
    melhor = get_close_matches(nome.upper(), órgãos_maíusculas.keys(), n=1, cutoff=0)[0]
    return órgãos_maíusculas[melhor], órgãos[órgãos_maíusculas[melhor]]

def nome_do_órgão(órgão: int) -> str:
    """Retorna o nome do órgão cujo código seja `órgão`"""
    try:
        return lista_de_órgãos()[órgão]
    except KeyError:
        return f'ÓRGÃO DESCONHECIDO ({órgão})'

# TODO: Tratar anexos em arquivos para evitar problemas de memória
def enviar_manifestação(conversa: Conversa) -> Tuple[int, str]:
    """Envia uma manifestação e retorna o número de protocolo"""
    nomes = re.compile(r'\/(?P<arquivo>[\w.]+?)\?')
    anexos = [
        {
            'NomeArquivo': nomes.search(anexo.url).group('arquivo'),
            'ConteudoZipadoEBase64': codecs.encode(
                gzip.compress(get(anexo.url).content),
                'base64'
            ).decode('utf-8'),
        }
        for anexo
        in conversa.get_anexos()
    ]
    quantidade_de_anexos = len(anexos)
    for anexo in anexos:
        anexo['TamanhoArquivo'] = len(anexo['ConteudoZipadoEBase64'])
    anexos[:] = [
        anexo
        for anexo
        in anexos
        if anexo['TamanhoArquivo'] < 30*1024*1024
    ]
    if len(anexos) != quantidade_de_anexos:
        anexo_removido = True
    else:
        anexo_removido = False
    anexos.append(
        {
            'NomeArquivo': 'histórico.txt',
            'ConteudoZipadoEBase64': codecs.encode(
                gzip.compress(bytes(conversa.get_conversa(), 'utf-8')),
                'base64'
            ).decode('utf-8')
        }
    )
    if 'test' not in sys.argv:
        resultado = EOUV['mantém manifestação'].RegistrarManifestacaoTerceiro(
            login=settings.EOUV_USUARIO,
            senha=settings.EOUV_SENHA,
            idTipoManifestacao=EOUV['tipos'][conversa.tipo],
            idOrgaoDestinatario=conversa.órgão,
            textoManifestacao=conversa.get_conteúdo(),
            nomeManifestante=' '.join(
                (conversa.usuário.first_name,
                conversa.usuário.last_name)
            ),
            idTipoIdentificacaoManifestante=3, # Identificado com restrição
            sexo=conversa.usuário.gender[0].upper(),
            email='manifestante@example.com',
            enviarEmailCidadao=0,
            anexosManifestacao={'AnexoManifestacao':anexos}
        )
        if resultado['CodigoErro']:
            raise EOUVError(f'{resultado["CodigoErro"]}: {resultado["DescricaoErro"]}')
        return resultado['Protocolo'], resultado['Url']
    return '00106.000001/2017-01', 'https://treinamentoouvidorias.cgu.gov.br/'
