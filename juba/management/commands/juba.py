"""Configura o robô do Juba no Facebook Messenger"""
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from mensageirofb import Config
from juba import GET_STARTED

class Command(BaseCommand):
    """Configura o Facebook Messenger"""

    help = 'Configura o Facebook Messenger'
    config = {
        "greeting": [
            {
                "locale": "default",
                "text": "Oi, eu sou o Juba! Sou o ouvidor do governo federal no Facebook Messenger. Vamos começar?"
            }
        ],
        "get_started": {
            "payload": GET_STARTED
        },
        # "persistent_menu": [
        #     {
        #         "locale": "default",
        #         "composer_input_disabled": False,
        #         "call_to_actions": [
        #             {
        #                 "type": "postback",
        #                 "title": "Registrar manifestação",
        #                 "payload": "JUBASTART"
        #             },
        #             {
        #                 "type": "postback",
        #                 "title": "Tirar dúvida",
        #                 "payload": "PITACOSTART"
        #             }
        #         ]
        #     }
        # ]
    }

    def add_arguments(self, parser):
        parser.add_argument(
            '--zerar',
            action='store_true',
            help='Zera a configuração do Facebook Messenger')

    def handle(self, *args, **options):
        """Realiza a configuração do Facebook Messenger"""
        configurador = Config(settings.JUBATOKEN)
        if not options['zerar']:
            self.stdout.write('Enviando informações para o Facebook…')
            r = configurador.set(**self.config)
            if 'result' in r and r['result'] == 'success':
                self.stdout.write('Ok')
            else:
                self.stderr.write('ERRO! Resposta do Facebook:')
                self.stderr.write(r)
                raise CommandError('Erro na solicitação de configuração do Facebook Messenger')
        else:
            self.stdout.write('Zerando configuração do Facebook Messenger…')
            r = configurador.erase()
            if 'result' in r and r['result'] == 'success':
                self.stdout.write('Ok')
            else:
                self.stderr.write('ERRO! Resposta do Facebook:')
                self.stderr.write(r)
                raise CommandError('Erro na solicitação de exclusão de configuração do Facebook Messenger')
