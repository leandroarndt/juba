from django.core.management.base import BaseCommand
from juba.models import Conversa

class Command(BaseCommand):
    def handle(self, *args, **options):
        for conversa in Conversa.objects.filter(protocolo='0'):
            conversa.protocolo=''
            conversa.save()