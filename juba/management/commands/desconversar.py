"""Fecha conversas antigas não terminadas"""
from django.core.management.base import BaseCommand
from django.conf import settings
from django.utils.timezone import datetime
from mensageirofb import TextMessage, TextQuickReply, Messenger
from juba import GET_STARTED
from juba.models import Conversa
from juba.eouv import enviar_manifestação
from juba.fluxo import enviar

class Command(BaseCommand):
    """Encerra conversas não concluídas"""

    help = 'Encerra conversas não concluídas'
    def handle(self, *args, **options):
        mensageiro = Messenger(token=settings.JUBATOKEN)
        # conversas = Conversa.objects.filter(ativa=True)\
        #             .exclude(fala__momento__gt=datetime.now()-settings.JUBA_TIMEDELTA)[:]
        for conversa in Conversa.objects.filter(ativa=True)\
                        .exclude(fala__momento__gt=datetime.now()-settings.JUBA_TIMEDELTA)\
                        .iterator():
            conversa.ativa = False
            enviar(
                TextMessage,
                psid=conversa.usuário.psid,
                text='Vejo que faz tempo que você não fala comigo… 😢',
                sender=mensageiro
            )
            enviar(
                TextMessage,
                psid=conversa.usuário.psid,
                text='Vou encerrar essa nossa conversa, porque preciso atender outras pessoas.',
                sender=mensageiro
            )
            if conversa.etapa == Conversa.CONTEÚDO_INSERIDO:
                conversa.protocolo, url = enviar_manifestação(conversa)
                enviar(
                    TextMessage,
                    psid=conversa.usuário.psid,
                    text=f'Seu protocolo é {conversa.protocolo}. '
                    f'Acesse {url} para consultar o andamento',
                    sender=mensageiro
                )
            enviar(
                TextMessage,
                psid=conversa.usuário.psid,
                text='Quando desejar entrar em contato novamente,'
                'clique no botão "Começar" abaixo:',
                quick_replies=[TextQuickReply(
                    title='Começar',
                    payload=GET_STARTED
                )],
                sender=mensageiro
            )
            conversa.save()

