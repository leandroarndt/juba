from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from django.conf import settings
from . import views

views.Webhook.mensageiro.test = True

class Webhook(TestCase):
    def setUp(self):
        self.url = reverse(':'.join((__package__, 'webhook')))
        self.client = Client()

    def test_status_code(self):
        print('{0}: status == 200'.format(self.__class__.__name__), end=' → ')
        response = self.client.get(self.url)
        try:
            self.assertEqual(response.status_code, 200)
            print('Ok')
        except AssertionError:
            print('FALHA! Status == {0}'.format(response.status_code))
            raise

    def test_token(self):
        """Testa a rotina de verificação do webhook pelo Facebook"""
        print('{0}: access token'.format(self.__class__.__name__), end=' → ')
        response = self.client.get(self.url, data={'hub.verify_token': settings.FBTOKEN,
                                                   'hub.challenge': 'ok'})
        try:
            self.assertEqual(response.content, bytes('ok', 'utf-8'))
            print('Ok')
        except AssertionError:
            print('FALHA! Resposta == {0}'.format(response.content))
            raise

    def tst_message(self):
        """Testa o tratamento de uma mensagem real"""
        conteúdo = """{"object":"page","entry":[{"id":"163963707518522","time":1507667388720,
        "messaging":[{"sender":{"id":"1582374501801465"},"recipient":{"id":"163963707518522"},
        "timestamp":1507667388135,"message":{"mid":"mid.$cAADP59d7HtNlOT_u51fB_qn3vJJx",
        "seq":10,"text":"Oi!"}}]}]}"""
        print('{0}: JSON'.format(self.__class__.__name__), end=' → ', flush=True)
        try:
            response = self.client.post(self.url, conteúdo, content_type='application/json')
            self.assertEqual(response.content, b'Success')
        except (KeyError, AssertionError):
            print('FALHA! Resposta == {0}'.format(response.content))
            raise
        print('Ok')

