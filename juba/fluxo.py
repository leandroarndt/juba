"""Processamento das mensagens do webhook de acordo com o fluxo
estabelecido."""
import logging
from pprint import pformat
from django.core.exceptions import ObjectDoesNotExist
from mensageirofb import AttachmentMessage, TextMessage,\
     TextQuickReply, LocationQuickReply, Message
from .eouv import obter_órgão, enviar_manifestação
from .models import Fala, Conversa
from . import GET_STARTED, ENCERRAR, \
    DENÚNCIA, ELOGIO, SOLICITAÇÃO, SUGESTÃO, RECLAMAÇÃO

LOGGER = logging.getLogger(__name__)

def enviar(cls, **kwargs):
    """Envia e salva mensagens

    *cls:* classe da mensagem
    *kwargs:* argumentos para criação da mensagem"""
    mensagem = cls(**kwargs)
    Fala.obter(mensagem, etapa=Fala.OUTRA)
    mensagem.send()

def encerrar(objeto: Conversa):
    """Encerra conversa

    **Não salva a conversa!**"""
    # if not isinstance(objeto, Conversa):
    #     objeto = Conversa.objects.get(usuário__pk=objeto)
    objeto.ativa = False
    objeto.protocolo, url = enviar_manifestação(objeto)
    return objeto.protocolo, url

def processar(storage, mensageiro):
    """Processa as mensagens armazenadas em `storage` de acordo
    com o fluxo estabelecido"""
    while storage.messages:
        message = storage.messages.pop(0)
        fala = Fala.obter(message)
        if fala.payload == GET_STARTED:
                fala.etapa = Fala.OUTRA
                fala.save()
                enviar(
                    TextMessage,
                    psid=message.sender,
                    text='Vamos lá, {nome}! '
                    'O que você deseja fazer?'.format(nome=fala.conversa.usuário.first_name),
                    quick_replies=[
                        TextQuickReply(
                            title='Denunciar',
                            payload=DENÚNCIA
                        ),
                        TextQuickReply(
                            title='Elogiar',
                            payload=ELOGIO
                        ),
                        TextQuickReply(
                            title='Solicitar',
                            payload=SOLICITAÇÃO
                        ),
                        TextQuickReply(
                            title='Sugerir',
                            payload=SUGESTÃO
                        ),
                        TextQuickReply(
                            title='Reclamar',
                            payload=RECLAMAÇÃO
                        )
                    ],
                    sender=mensageiro
                )
        elif fala.payload in (DENÚNCIA, ELOGIO, SOLICITAÇÃO, SUGESTÃO, RECLAMAÇÃO):
            fala.etapa = Fala.TIPO
            fala.save()
            fala.conversa.etapa = Conversa.TIPO
            if fala.payload == DENÚNCIA:
                fala.conversa.tipo = Conversa.DENÚNCIA
                texto = '🤔 Compreendo. Em qual órgão público aconteceu o ilícito?'
            elif fala.payload == ELOGIO:
                fala.conversa.tipo = Conversa.ELOGIO
                texto = 'Que bom! 😀 A que órgão público se refere?'
            elif fala.payload == SOLICITAÇÃO:
                fala.conversa.tipo = Conversa.SOLICITAÇÃO
                texto = '👍 Vamos lá! Para qual órgão público se destina sua solicitação?'
            elif fala.payload == SUGESTÃO:
                fala.conversa.tipo = Conversa.SUGESTÃO
                texto = '👍 Muito bem! Para qual órgão público você deseja fazer sua sugestão?'
            else:
                fala.conversa.tipo = Conversa.RECLAMAÇÃO
                texto = '😮 Que pena. Você deseja reclamar para qual órgão público?'
            fala.conversa.save()
            # TODO: lista de ógãos. Webiew?
            enviar(
                TextMessage,
                psid=message.sender,
                text=texto,
                sender=mensageiro
            )
        elif fala.payload == ENCERRAR:
            protocolo, url = encerrar(fala.conversa)
            enviar(
                TextMessage,
                psid=message.sender,
                text='Obrigado pela sua manifestação! '
                f'Seu protocolo é {protocolo}. '
                f'Acesse {url} para consultar o andamento',
                sender=mensageiro
            )
            enviar(
                TextMessage,
                psid=message.sender,
                text='Quando desejar entrar em contato novamente,'
                'clique no botão "Começar" abaixo:',
                quick_replies=[TextQuickReply(
                    title='Começar',
                    payload=GET_STARTED
                )],
                sender=mensageiro
            )
            fala.conversa.save()
        elif fala.payload:
            enviar(
                TextMessage,
                text='Desculpe-me, ainda não sei fazer isso.'
                'Deseja recomeçar?',
                psid=message.sender,
                quick_replies=[TextQuickReply(
                    title='Recomeçar',
                    payload=GET_STARTED
                )],
                sender=mensageiro
            )
        elif fala.tipo == Message.TEXT and fala.conversa.etapa == Conversa.TIPO:
            fala.etapa = Fala.ÓRGÃO
            fala.save()
            fala.conversa.órgão = obter_órgão(fala.texto)[0]
            fala.conversa.etapa = Conversa.ÓRGÃO
            fala.conversa.save()
            enviar(
                TextMessage,
                text='Ok. O que você deseja relatar?',
                psid=message.sender,
                sender=mensageiro
            )
        elif \
        (
            fala.tipo == Message.TEXT or \
            fala.tipo == Message.ATTACHMENTS
        ) and \
        (
            fala.conversa.etapa == Conversa.ÓRGÃO or \
            fala.conversa.etapa == Conversa.CONTEÚDO_INSERIDO
        ):
            fala.etapa = Fala.CONTEÚDO
            fala.save()
            fala.conversa.etapa = Conversa.CONTEÚDO_INSERIDO
            fala.conversa.save()
            enviar(
                TextMessage,
                text='Estou anotando, pode continuar. '
                'Quando tiver terminado, pressione "Encerrar"',
                quick_replies=[TextQuickReply(
                    title='Encerrar',
                    payload=ENCERRAR
                )],
                psid=message.sender,
                sender=mensageiro
            )
        else:
            # TODO: verificar a etapa e continuar a interação
            LOGGER.error('Mensagem não compreendida:\n{mensagem}'.format(
                mensagem=pformat(message.data)
            ))
            enviar(
                TextMessage,
                text='Desculpe-me, mas não compreendi o que você disse.',
                psid=message.sender,
                sender=mensageiro
            )
            if fala.conversa.etapa == Conversa.ÓRGÃO or \
            fala.conversa.etapa == Conversa.CONTEÚDO_INSERIDO:
                enviar(
                    TextMessage,
                    text='Mesmo assim, pode continuar. '
                    'Quando tiver terminado, pressione "Encerrar"',
                    quick_replies=[TextQuickReply(
                        title='Encerrar',
                        payload=ENCERRAR
                    )],
                    psid=message.sender,
                    sender=mensageiro
                )
            else:
                enviar(
                    TextMessage,
                    text='Vamos recomeçar?',
                    psid=message.sender,
                    quick_replies=[TextQuickReply(
                        title='Recomeçar',
                        payload=GET_STARTED
                    )],
                    sender=mensageiro
                )
