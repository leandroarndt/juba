'''
Created on 25 de set de 2017

@author: Arndt
'''

from django.views.generic import View, TemplateView
from django.http import HttpResponseBadRequest, HttpResponseServerError, HttpResponse
# from django.template import RequestContext
from django.template.context_processors import csrf
from django.utils.cache import add_never_cache_headers
from django.conf import settings
from urllib.request import Request, urlopen
from urllib.parse import urlencode
from urllib.error import URLError
import logging

logger = logging.getLogger(__name__)

class Pergunta(View):
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        if 'question' in request.POST:
            requisicao = Request(
                'https://westus.api.cognitive.microsoft.com/qnamaker/v1.0/knowledgebases/' \
                +settings.KBID+'/generateAnswer',
                data=bytes('{"question": "'+request.POST['question']+'"}', 'utf-8'),
                headers={'Ocp-Apim-Subscription-Key': settings.SUBSCRIPTION_KEY,
                        'Content-Type': 'application/json',
                        'Cache-Control': 'no-cache'},
                origin_req_host='https://westus.api.cognitive.microsoft.com/qnamaker/v1.0')
            #requisicao.add_header('Ocp-Apim-Subscription-Key', settings.SUBSCRIPTION_KEY)
            #requisicao.add_header('Content-Type', 'application/json')
            #requisicao.add_header('Cache-Control', 'no-cache')
            #requisicao.data = urlencode({"question": request.POST['question']}).encode('utf-8')
            try:
                resposta = urlopen(requisicao, timeout=30)
                resposta = resposta.read()
                resposta = HttpResponse(resposta, content_type='application/json')
                add_never_cache_headers(resposta)
                return resposta
            except URLError as e:
                logger.error(e.reason)
                logger.error(e.read())
                return HttpResponseServerError('Desculpe, minha mem&oacute;ria falhou. Que tal perguntar pelo [e-OUV](https://sistema.ouvidorias.gov.br/)?')
        else:
            return HttpResponseBadRequest()

class CSRFTemplateView(TemplateView):
    def get(self, request, *args, **kwargs):
        contexto = self.get_context_data(**kwargs)
        contexto['csrf_token']=  csrf(request)['csrf_token']
        return super(CSRFTemplateView, self).get(request, **contexto)