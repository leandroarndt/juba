from django.test import TestCase, Client
from django.core.urlresolvers import reverse

class Início(TestCase):
    """Teste da página inicial"""
    def setUp(self):
        """Configura o teste"""
        self.url = reverse(':'.join((__package__,'inicio')))
        self.client = Client()

    def test_status_code(self):
        """Deve retornar 200"""
        print('{0}: status == 200'.format(self.__class__.__name__), end=' → ')
        response = self.client.get(self.url)
        try:
            self.assertEqual(response.status_code, 200)
            print('Ok')
        except AssertionError:
            print('FALHA! Status == {0}'.format(response.status_code))

class Pergunta(TestCase):
    """Teste do mecanismo de perguntas e respostas"""
    def setUp(self):
        """Configura o teste"""
        self.url = reverse(':'.join((__package__,'pergunta')))
        self.client = Client(enforce_csrf_checks=False)

    def test_get(self):
        """Testa o método GET (deve retornar _forbidden_)"""
        print('{0}: get status == 405'.format(self.__class__.__name__), end=' → ')
        response = self.client.get(self.url)
        try:
            self.assertEqual(response.status_code, 405)
            print('Ok')
        except AssertionError:
            print('FALHA! Status == {0}'.format(response.status_code))

    def test_post(self):
        """Testa o método POST (único permitido)"""
        print('{0}: get status == 200'.format(self.__class__.__name__), end=' → ')
        response = self.client.post(self.url, {'question': "Oi!"})
        try:
            self.assertEqual(response.status_code, 200)
            print('Ok')
        except AssertionError:
            print('FALHA! Status == {0}'.format(response.status_code))
