from django.conf.urls import url, include
from .views import Pergunta, CSRFTemplateView

urlpatterns = [
    url(r'^$', CSRFTemplateView.as_view(template_name='inicio.html'), name='inicio', ),
    url(r'^pergunta$', Pergunta.as_view(), name='pergunta'),
]